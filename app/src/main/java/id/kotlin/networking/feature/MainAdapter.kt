package id.kotlin.networking.feature

import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import android.view.ViewGroup
import id.kotlin.networking.R
import id.kotlin.networking.data.Response
import id.kotlin.networking.feature.MainAdapter.MainHolder
import id.kotlin.networking.inflate
import kotlinx.android.synthetic.main.item_main.view.*

class MainAdapter(private val data: List<Response>) : Adapter<MainHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder = MainHolder(parent.inflate(R.layout.item_main))

    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        holder.bind(data[holder.adapterPosition])
    }

    override fun getItemCount(): Int = data.size

    inner class MainHolder(itemView: View) : ViewHolder(itemView) {

        fun bind(data: Response) {
            with(itemView) {
                tv_title.text = data.title
                tv_overview.text = data.overview
            }
        }
    }
}