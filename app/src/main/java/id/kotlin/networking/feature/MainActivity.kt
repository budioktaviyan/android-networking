package id.kotlin.networking.feature

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.kotlin.networking.App
import id.kotlin.networking.AppConfig
import id.kotlin.networking.R
import id.kotlin.networking.data.Datasource
import id.kotlin.networking.data.Response
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val datasource: Datasource = App.provideRetrofit().create(Datasource::class.java)
        val disposable: Disposable = datasource.discoverMovie(AppConfig.API_KEY, "popularity.desc")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { value ->
                    val data: List<Response> = value.responses.map { it }
                    val adapter = MainAdapter(data)
                    rv_main.adapter = adapter
                }
        disposables.add(disposable)
    }
}