package id.kotlin.networking

import android.app.Application
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class App : Application() {

    companion object {

        fun provideRetrofit(): Retrofit = Retrofit.Builder().apply {
            client(getOkHttpClient())
            baseUrl(AppConfig.BASE_URL)
            addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            addConverterFactory(GsonConverterFactory.create())
        }.build()

        private fun getOkHttpClient(): OkHttpClient = OkHttpClient.Builder().apply {
            retryOnConnectionFailure(true)
        }.build()
    }
}