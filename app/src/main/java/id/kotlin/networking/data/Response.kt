package id.kotlin.networking.data

import com.google.gson.annotations.SerializedName

data class Data(
        @SerializedName("results") val responses: List<Response>
)

data class Response(
        @SerializedName("title") val title: String,
        @SerializedName("overview") val overview: String
)