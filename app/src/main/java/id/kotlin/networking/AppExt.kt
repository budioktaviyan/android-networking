package id.kotlin.networking

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflate(layoutRes: Int): View =
        LayoutInflater.from(this.context).inflate(layoutRes, this, false)